//
//  commandDefinition.h
//  
//
//  Created by Napoleon Leoni on 9/1/14.
//
//

#ifndef _commandDefinition_h

void runM1(char*,const char*);
void turnMotorOff(char*,const char*);
void turnMotorOn(char*,const char*);
void resetEncoder(char*,const char*);
void ControlLoop(char*,const char*);
void setPID(char*,const char*);
void setProfile(char*,const char*);
void setTarget(char*,const char*);

const int nFunctions=8; //NUmber of functions in the function Map.
const functionMap myMap[] = {
    { "runm1", runM1 , "\n>"},//call with numeric parameter (-400,400) setting proportional voltage to motor
    { "motoroff", turnMotorOff,"\n>"},
    { "motoron", turnMotorOn,"\n>"},
    { "resetEncoder",resetEncoder,"\n"},
    { "control", ControlLoop,"\n"},//Should be called with 1 to enable and 0 to disable: e.g. control-1-
    { "pid", setPID,"\n"},//Call as pid -p<kp> -i<ki> -d<kd> -
    { "setProfile", setProfile, "\n" },
    { "setTarget", setTarget, "\n"} //Set Target Position in encoder counts(for now)
    
};

#define _commandDefinition_h
#endif

