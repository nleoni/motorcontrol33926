#include <CommandParserC.h>
#include <Encoder.h>
#include <DualMC33926MotorShield.h>
#include "commandDefinition.h"

#define MOTOROFF          0x0000
#define MOTORON           0X0001
#define CONTROLON         0X0010
#define PROFILEINPROGRESS 0x0100

//Define motor driver  instance
DualMC33926MotorShield md;
const unsigned int motorEnablePin=4;

unsigned int motorState=MOTOROFF;//motor state variable


char stringCommand[MAXCOMMANDLENGTH];
char myDelimiter[]="- \n";
boolean newSerialData=false;

long encoderPosition=0;
long targetPosition=0;

Encoder motorEncoder(2,3);
int counter=0;
bool printFlag=0;
int motorPotential=0;
unsigned int motorCurrentmAmps;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Motor Control");
  md.init();
 printCommandHelp(myMap,nFunctions,myDelimiter);
   // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function below
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);
  //Exact frequency is 976.5625 Hz

}

// Interrupt is called once a millisecond, 
SIGNAL(TIMER0_COMPA_vect) 
{
counter++;
  if(counter>=50){
    counter=0;
    printFlag=1;
    encoderPosition=motorEncoder.read();
    motorCurrentmAmps=md.getM1CurrentMilliamps();
  }
}

void loop() {
  //Mostly check for serial commamnds
  processSerialCommand(stringCommand,&newSerialData);
  if(newSerialData){
    processCommand(myMap,nFunctions,myDelimiter,stringCommand);
   newSerialData=false; 
  }
  
  /*if(printFlag){
    Serial.print(encoderPosition);
    Serial.print(",");
    Serial.print(motorCurrentmAmps);
    Serial.print(",");
    Serial.print(motorPotential);
    Serial.print("\n");
    printFlag=0;
  }*/
  
}

//**********************************************************************
//All functions defined below are associated wth the commandDefinition file and they all represent a callback to a  command that can be 
//called through the serial port
void runM1(char*command,const char*delim){
//Read number from command, verify range is -400 to 400
int i=0;
      if(command!=NULL){
      Serial.print("Run Motor called with command:");
      Serial.println(command);
      i=atoi(command);
 //set motor speed to read value
      Serial.print(F(">Run motor 1 called with speed:"));
      Serial.print(i);
      Serial.print("\n");
      motorPotential=i;
      md.setM1Speed(i);
      } else {
      Serial.print(F("Proper use is: runm1-speed- where -400<speed<400\n"));
      }
     return;
}


void turnMotorOff(char*command,const char*delim){
//disable motor

     Serial.print(F("Called Disable Motors\n"));
     digitalWrite(motorEnablePin,LOW); // default to off;
     return;
}

void turnMotorOn(char*command,const char*delim){
//disable motor

     Serial.print(F("Called Enable Motors\n"));
     digitalWrite(motorEnablePin,HIGH); // default to on;
     return;
}

void resetEncoder(char*command,const char*delim){
//reset Encoder position, this also resets the target position
//for the control Loop

     Serial.print(F("Called Reset Encoder\n"));
     motorEncoder.write(0);//Reset Encoder
     encoderPosition=0;
     targetPosition=0;
     return;
}

void ControlLoop(char*command,const char*delim){
  
  while(command!=NULL){
  //read and parse current command

  //read next command
  command=strtok(NULL,delim);
  }
}

void setPID(char*command,const char*delim){
  char* number;
  double value;
  while(command!=NULL){
  //read and parse current command
  //Should go over white spaces first
  //read next command
  number=strstr(command,"i");
  if(number!=NULL){
    number++;
    value=atof(number);
     Serial.print(F("Set Ki="));
     Serial.println(value);
  } else {
    number=strstr(command,"d");
      if(number!=NULL){
      number++;
      value=atof(number);
      Serial.print(F("Set Kd="));
      Serial.println(value);
      } else {
      number=strstr(command,"p");
        if(number!=NULL){
          number++;
          value=atof(number);
          Serial.print(F("Set Kp="));
          Serial.println(value);
          } else {
          //print error message
          return;
          }
      }
    }
  command=strtok(NULL,delim);
  }

}

void setProfile(char*command,const char*delim){

  
}

void setTarget(char*command,const char*delim){
  
  
}

//**********************************************************************

